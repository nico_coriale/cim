from django.test import TestCase
from django.template import loader
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model


from be.core.decorator import response_handler

class ResponseTestCase(TestCase):

    user = get_user_model()

    """
        Suite di Test response
            1 - Commentare entrypoint di Dokcerfile
            2 - cd usr/src/app/cim &&  python3.5 manage.py test --keepdb be.test.views.test_response.ResponseTestCase.<nome_metodo>
    """

    def test_200(self):
        response = self.client.get('/test200/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'prova')

    def test_301(self):
        response = self.client.get('/test200')
        print(response.content)
        self.assertEqual(response.status_code, 301)

    @response_handler
    def test_200_response(self):
        return "prova"

    def test_200_parameter(self):
        response = self.client.get('/test200parameter/1234/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'1234')

    @response_handler
    def test_200_parameter_response(self, name=None):
        return name

    def test_200_argument(self):
        response = self.client.get('/test200argument/?name=1234')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'1234')

    @response_handler
    def test_200_parameter_argument(self):
        name = self.GET.get('name')
        return name

    def test_404(self):
        response = self.client.get('/page/not/found/')
        self.assertEqual(response.status_code, 404)

    def test_500(self):
        response = self.client.get('/test500/')
        self.assertEqual(response.status_code, 500)

    @response_handler
    def test_500_response(self):
        raise Exception

    def test_403(self):
        response = self.client.get('/test403/')
        print(response)
        self.assertEqual(response.status_code, 403)

    @response_handler
    def test_403_response(self):
        return PermissionDenied("No logged in user")

    def test_302(self):
        response = self.client.get('/test302/')
        self.assertEqual(response.status_code, 302)

    @login_required()
    @response_handler
    def test_302_response(self):
        #login_required fa un redirect
        return

