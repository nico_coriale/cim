from django.db import models

from be.test.manager.anagrafica import AnagraficaManager
from be.core.models.core import Core

class Anagrafica(Core):

    id_anagrafica_prova = models.IntegerField(max_length=11, primary_key=True)
    codice_prova = models.IntegerField(max_length=11)
    nome_prova = models.CharField(max_length=255)
    prezzo_base = models.FloatField()
    tipo = models.CharField(max_length=30)
    obbligatoria = models.CharField(max_length=1)
    descrizione_prova = models.CharField(max_length=255)

    objects = AnagraficaManager()

    class Meta:
        db_table = 'anagrafica_prove_cim'
        app_label = 'ana_cim'


