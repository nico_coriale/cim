'''
Filtrando questa lista per i valori inseriti in template_articoli, ottengo un solo risultato (se esiste).
Dato l'id del template risultante, recupero da @prove_articoli_per_template le prove associate.
'''
template_articoli_list = [
    {
        "id_template": 1,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2012-02-22 12:28:23",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2012 A/I"
    },
    {
        "id_template": 2,
        "gm": "10",
        "segmento": "produzione",
        "anno_stagione": "2012-01-27 10:22:50",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2012 A/I"
    },
    {
        "id_template": 3,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2012-02-15 10:32:45",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2012 A/I"
    },
    {
        "id_template": 4,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2012-03-08 09:06:58",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2013 P/E"
    },
    {
        "id_template": 5,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2012-03-07 15:44:59",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2013 P/E"
    },
    {
        "id_template": 6,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2012-09-28 13:34:24",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2013 A/I"
    },
    {
        "id_template": 7,
        "gm": "10",
        "segmento": "produzione",
        "anno_stagione": "2013-03-08 10:35:52",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2013 A/I"
    },
    {
        "id_template": 8,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2012-09-24 12:52:04",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2013 A/I"
    },
    {
        "id_template": 9,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2013-03-27 09:21:43",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2014 P/E"
    },
    {
        "id_template": 10,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2013-03-27 09:22:21",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2014 P/E"
    },
    {
        "id_template": 11,
        "gm": "10",
        "segmento": "produzione",
        "anno_stagione": "2013-03-27 09:22:53",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2014 P/E"
    },
    {
        "id_template": 12,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2013-10-10 15:43:56",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2014 A/I"
    },
    {
        "id_template": 13,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2013-10-15 07:12:25",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2014 A/I"
    },
    {
        "id_template": 14,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2014-01-23 08:35:10",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2015 P/E"
    },
    {
        "id_template": 15,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2014-04-14 06:35:57",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2011 P/E"
    },
    {
        "id_template": 16,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2014-04-14 06:37:32",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2015 P/E"
    },
    {
        "id_template": 17,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2014-06-23 09:32:33",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2015 A/I"
    },
    {
        "id_template": 18,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2014-06-23 09:33:24",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2015 A/I"
    },
    {
        "id_template": 19,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2015-01-21 08:14:04",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2016 P/E"
    },
    {
        "id_template": 20,
        "gm": "80",
        "segmento": "campionario",
        "anno_stagione": "2015-05-13 10:39:49",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2016 P/E"
    },
    {
        "id_template": 21,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2015-09-01 11:55:40",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2016 A/I"
    },
    {
        "id_template": 22,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2016-03-17 16:05:51",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2017 P/E"
    },
    {
        "id_template": 23,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2016-09-15 12:09:13",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2017 A/I"
    },
    {
        "id_template": 24,
        "gm": "10",
        "segmento": "",
        "anno_stagione": "2016-10-28 07:11:19",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2015 P/E"
    },
    {
        "id_template": 25,
        "gm": "15",
        "segmento": "",
        "anno_stagione": "2017-05-09 06:37:15",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2017 A/I"
    },
    {
        "id_template": 26,
        "gm": "15",
        "segmento": "",
        "anno_stagione": "2016-11-22 14:26:35",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "permanente"
    },
    {
        "id_template": 27,
        "gm": "10",
        "segmento": "",
        "anno_stagione": "2016-11-29 13:07:37",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2017 A/I"
    },
    {
        "id_template": 28,
        "gm": "10",
        "segmento": "campionario",
        "anno_stagione": "2017-03-29 08:23:20",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2018 P/E"
    },
    {
        "id_template": 29,
        "gm": "10",
        "segmento": "",
        "anno_stagione": "2017-04-13 15:03:28",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2018 P/E"
    },
    {
        "id_template": 30,
        "gm": "10",
        "segmento": "",
        "anno_stagione": "2017-11-15 14:18:28",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2018 A/I"
    },
    {
        "id_template": 32,
        "gm": "10",
        "segmento": "",
        "anno_stagione": "2018-05-24 13:57:20",
        "upd_ts": "0000-00-00 00:00:00",
        "upd_user": "2019 P/E"
    }
]

prove_articoli_per_template = [
    {
        "id_template": 1,
        "prove": [
            {
                "id_anagrafica_prova": 42,
                "nome_prova": "StabilitÃ  dimensionale al Vapore + Lavaggio ad acqua + stiro"
            },
            {
                "id_anagrafica_prova": 22,
                "nome_prova": "Scorrimento delle cuciture"
            },
            {
                "id_anagrafica_prova": 23,
                "nome_prova": "Resistenza alla trazione"
            },
            {
                "id_anagrafica_prova": 29,
                "nome_prova": "Resistenza al pilling - Pilling Box"
            },
            {
                "id_anagrafica_prova": 28,
                "nome_prova": "Resistenza al pilling - Martindale"
            },
            {
                "id_anagrafica_prova": 8,
                "nome_prova": "PH"
            },
            {
                "id_anagrafica_prova": 9,
                "nome_prova": "PFOS & PFOA"
            },
            {
                "id_anagrafica_prova": 2,
                "nome_prova": "Formaldeide"
            },
            {
                "id_anagrafica_prova": 7,
                "nome_prova": "Coloranti disp. allerg./canc."
            },
            {
                "id_anagrafica_prova": 1,
                "nome_prova": "Ammine aromatiche"
            },
            {
                "id_anagrafica_prova": 6,
                "nome_prova": "Ammine A. estraibili da fibre (PL)"
            }
        ]
    },
    {
        "id_template": 3,
        "prove": [
            {
                "id_anagrafica_prova": 1,
                "nome_prova": "Ammine aromatiche"
            },
            {
                "id_anagrafica_prova": 23,
                "nome_prova": "Resistenza alla trazione"
            },
            {
                "id_anagrafica_prova": 22,
                "nome_prova": "Scorrimento delle cuciture"
            },
            {
                "id_anagrafica_prova": 28,
                "nome_prova": "Resistenza al pilling - Martindale"
            },
            {
                "id_anagrafica_prova": 2,
                "nome_prova": "Formaldeide"
            }
        ]
    },
    {
        "id_template": 4,
        "prove": [
            {
                "id_anagrafica_prova": 42,
                "nome_prova": "StabilitÃ  dimensionale al Vapore + Lavaggio ad acqua + stiro"
            },
            {
                "id_anagrafica_prova": 22,
                "nome_prova": "Scorrimento delle cuciture"
            },
            {
                "id_anagrafica_prova": 23,
                "nome_prova": "Resistenza alla trazione"
            },
            {
                "id_anagrafica_prova": 8,
                "nome_prova": "PH"
            },
            {
                "id_anagrafica_prova": 2,
                "nome_prova": "Formaldeide"
            },
            {
                "id_anagrafica_prova": 1,
                "nome_prova": "Ammine aromatiche"
            },
            {
                "id_anagrafica_prova": 9,
                "nome_prova": "PFOS & PFOA"
            }
        ]
    },
    {
        "id_template": 6,
        "prove": [
            {
                "id_anagrafica_prova": 22,
                "nome_prova": "Scorrimento delle cuciture"
            },
            {
                "id_anagrafica_prova": 23,
                "nome_prova": "Resistenza alla trazione"
            },
            {
                "id_anagrafica_prova": 28,
                "nome_prova": "Resistenza al pilling - Martindale"
            },
            {
                "id_anagrafica_prova": 1,
                "nome_prova": "Ammine aromatiche"
            },
            {
                "id_anagrafica_prova": 42,
                "nome_prova": "StabilitÃ  dimensionale al Vapore + Lavaggio ad acqua + stiro"
            }
        ]
    }
]