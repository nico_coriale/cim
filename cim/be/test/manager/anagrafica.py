from django.db import models
from be.core.manager.core import CoreManager

class AnagraficaManager(CoreManager):

    def getAnagraficaByApplicaSuColori1(self, applica_su_colori):

        query = """
                SELECT codice_prova, nome_prova, prezzo_base
                    FROM anagrafica_prove_cim
                    WHERE applica_su_colori = '{}'
                """.format(applica_su_colori)

        result_list = []
        for row in self.execute_query_custom(query):
            p = self.model(codice_prova=row[0], nome_prova=row[1], prezzo_base=row[2])
            result_list.append(p)
        return result_list

    def getAnagraficaByApplicaSuColori(self, applica_su_colori):

        query = """
                SELECT id_anagrafica_prova, codice_prova, nome_prova, prezzo_base
                    FROM anagrafica_prove_cim
                    WHERE applica_su_colori = '{}'
                    LIMIT 3
                """.format(applica_su_colori)

        return self.execute_query_custom_model(query)
