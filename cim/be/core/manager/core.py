import logging.config
from django.db import models, connection
from django.forms.models import model_to_dict

log = logging.getLogger('cim')

class CoreManager(models.Manager):

    @classmethod
    def execute_query_custom(cls, query):
        with connection.cursor() as cursor:
            cursor.execute(query)
            return cursor.fetchall()

    def execute_query_custom_model(self, query, name='CustomModel'):
        list_custom_model = []
        try:
            res =  self.model.objects.raw(query)
            if res:
                pk = 1
                for p in res:
                    fields = model_to_dict(p, fields=p.__dict__.keys())
                    custom_model = self.model._create_custom_model(name, pk, fields=fields)
                    list_custom_model.append(custom_model)
                    pk +=1
            return list_custom_model
        except Exception as Ex:
            log.error(Ex)
            raise Exception("Query non corretta")


    @classmethod
    def map_result_query_custom(cls, result, key_list):
        result_list = []
        for row in result:
            p = cls.model(codice_prova=row[0], nome_prova=row[1], prezzo_base=row[2])
            result_list.append(p)

        return result_list


    def getAnagraficaByApplicaSuColori(self, applica_su_colori):
        with connection.cursor() as cursor:
            query = """
                    SELECT codice_prova, nome_prova, prezzo_base
                        FROM anagrafica_prove_cim
                        WHERE applica_su_colori = '{}'
                    """.format(applica_su_colori)
            cursor.execute(query)
            result_list = []
            for row in cursor.fetchall():
                p = self.model(codice_prova=row[0], nome_prova=row[1], prezzo_base=row[2])
                result_list.append(p)
        return result_list