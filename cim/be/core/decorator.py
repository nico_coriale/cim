from django.http import HttpResponse, HttpResponseServerError, HttpResponseForbidden
from django.utils.decorators import available_attrs
from functools import update_wrapper, wraps

import json, urllib, logging, traceback, sys, time

log = logging.getLogger('cim')

def response_handler(f):
    """
        Decoratore per gestione errori
    """

    def response_handler_f(*args, **kwargs):

        try:
            request = args[1].__dict__
        except IndexError:
            request = args[0].__dict__

        request.setdefault('REPLY_HEADERS', {})
        request['REPLY_HEADERS']['Cache-Control'] = 'no-cache'
        request['REPLY_HEADERS']['Expires'] = '-1'

        try:
            start = time.time()
            result_raw = f(*args, **kwargs)

            if type(result_raw) == list:
                for res in result_raw:
                    if type(res) == dict:
                        res['status'] = 'SUCCESS'
            elif type(result_raw) == dict:
                result_raw['status'] = 'SUCCESS'

            result = result_raw
            end = time.time()
            #logger.error("perf;" + request.get("REMOTE_ADDR", "") + ";" + request.get("module", "") + "." + request.get("program", "") + "." + request.get("function", "") + ";" + str(tempo))

            return HttpResponse(result)

        # except ApplicationError as ae:
        #
        #     request['REPLY_STATUS'] = "500 KO"
        #     result_raw = {'status': 'ERROR', 'message': str(ae)}
        #     result = json.dumps(result_raw)
        #
        #     return HttpResponseServerError(result)

        except Exception as e:

            log.exception(e)

            request['REPLY_STATUS'] = "500 KO"
            exc_type, exc_value, exc_tb = sys.exc_info()
            #logger.error(print_exeption(exc_type, exc_value, exc_tb))

            msg = str(e)
            if msg == "":
                msg = "Servizio al momento non disponibile, contattare l' assistenza"
            result_raw = {'status': 'ERROR', 'message': msg}
            result = json.dumps(result_raw)

            return HttpResponseServerError(result)

    return response_handler_f


def print_exeption(exc_type, exc_value, exc_tb):
    l = traceback.format_exception(exc_type, exc_value, exc_tb)
    a = []
    for e in l:
        a.append("\n" + str(e))

    return ''.join(a)
