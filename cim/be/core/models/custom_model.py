from ..models.core import Core

class CustomModel(Core):

    class Meta:
        db_table = 'anagrafica_prove_cim'
        app_label = 'custom_model_cim'
