from django.core import serializers
from django.db.models.query import RawQuerySet

def get_model_JSON(obj):

    if isinstance(obj, list) or isinstance(obj, tuple):
        serialized_obj = serializers.serialize('json', obj)
    else:
        serialized_obj = serializers.serialize('json', [obj, ])

    return serialized_obj
