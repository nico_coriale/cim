import logging.config
import json

from django.views import View

from be.core.decorator import response_handler
from be.test.stub import template_articoli
log = logging.getLogger('cim')

class CimView(View):

    # anagraficaService = AnagraficaService()

    @response_handler
    def get_template_articoli(self):

        # anagrafica = self.anagraficaService.getAnagraficaByApplicaSuColori("quals")

        # return utils.get_model_JSON(template_articoli.template_articoli_list)
        return json.dumps(template_articoli.template_articoli_list)
