import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { GestioneTemplateComponent } from '../gestione-template/gestione-template.component';
const routes: Routes = [
  { path: '', component: LayoutComponent, children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'contact/:id', loadChildren: '../contacts/contact.module#ContactModule' },
    { path: 'contacts', loadChildren: '../contacts/contacts.module#ContactsModule' },
    { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
    { path: 'another-page', loadChildren: '../another/another.module#AnotherModule' },
    //{ path: 'gestione-template', loadChildren: '../gestione-tempate/gestione-template.module#GestioneTemplateModule' },
    {
      path: 'gestione-template',
      component: GestioneTemplateComponent,      
    }
  ]}
];

export const ROUTES = RouterModule.forChild(routes);
