import { Component, TemplateRef } from '@angular/core';
import { CimApiService } from '../services/cim.service';
import { DndDropEvent } from 'ngx-drag-drop';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


declare let jQuery: any;


@Component({
    selector: 'app-another',
    templateUrl: './another.template.html',
    styleUrls: ['./another.component.css']

})
export class AnotherComponent {

    rForm: FormGroup;
    modalRef: BsModalRef;
    myControl = new FormControl();

    constructor(private _demoService: CimApiService, private modalService: BsModalService) {

    }
    ngOnInit() {

    }

    public isLoading: boolean = false;
    public attivo = false;

    public isError: boolean = false;

    public listaTemplate = [];
    message: string;

    private anniStagione = [];

    /**
     * Memorizza solamente l'id delle selezioni degli autocomplete (i cui valori 
     * sono nella forma "ID - <descrizione>")
     */
    idPreso = "";

    attivaBtnCerca = false;

    /**
     * Variabili temporanee, in attesa di poter comunicare col backend.
     */


    arrayModelli = [];
    arrayArticoli = [];
    arrayImputazioni = [];

    gruppo?: string;
    societa?: string;
    classe?: string;
    gm?: string;


    confAnnoStagione = {
        defaultValue: "Anno Stagione",
        values: this.anniStagione,
        includeAllItem: false,
        required: true,
        campo: "valore",
    };

    confTipoProva = {};
    confSegmentiModelli = {};
    confSegmentiArticoli = {};

    confSocieta = {
        name: "societa",
        defaultValue: "Max Mara",
        values: [{ valore: "Max Mara" }],
        includeAllItem: false,
        required: true,
    };
    confClasse = {
        nome: "classe",
        placeholder: 'Classe',
        values: [
            "01 - Cappotto",
            "04 - Giacca",
            "10 - Gonna",
            "22 - Abito",
            "46 - Pelliccia",
        ],
        required: true,
        template: "templateClasse",
    };

    confGM = {
        nome: "gm",
        placeholder: "GM",
        values: [
            "10 - Tessuto",
            "13 - Jersey",
            "29 - Lampo",
            "56 - Piumino",
            "84 - Tessuto per accessori",
            "81 - Pelle scarpe e borse"
        ],
        required: true,
        template: "templateGM",
    };

    confImputazione = {};


    svuotaTemplateModelli = () => {
        jQuery('#tabTemplateModelli tbody tr').each(function () {
            jQuery('#tabProveModelli tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateModelli').addClass('is-hidden');
    }
    svuotaTemplateArticoli = () => {
        jQuery('#tabTemplateArticoli tbody tr').each(function () {
            jQuery('#tabProveArticoli tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateArticoli').addClass('is-hidden');
    }
    svuotaTemplateImputazioni = () => {
        jQuery('#tabTemplateImputazioni tbody tr').each(function () {
            jQuery('#tabProveImputazioni tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateImputazioni').addClass('is-hidden');
    }

    //  ------------------metodi relativi al DRAG & DROP 

    draggable = {
        data: "myDragData",
        effectAllowed: "move",
        disable: false,
        handle: false,
    };

    drag = (ev) => {
        //this.idPreso = ev.dataTransfer.setData("text", ev.target.id);
    }
    /**
     * Sposta la riga draggata nel tbody della tabella sulla quale è rilasciata.
     * 
     * N.B. Le righe delle tabelle DEVONO avere un ID, altrimenti non funziona.
     */
    onDrop = (ev) => {
        jQuery("#" + this.idPreso).toggleClass('modifica');
        jQuery(ev.event.target).closest('table').find('tbody').append(
            jQuery("#" + this.idPreso)
        );
    }
    onDragStart = (event) => {
        console.log("drag started", JSON.stringify(event, null, 2));
        this.idPreso = event.srcElement.getAttribute("id");

    }
    onDragEnd = (event: DragEvent) => {
        console.log("drag ended", JSON.stringify(event, null, 2));
    }
    onDragover = (event: DragEvent) => {
    }
    // ----------------------------------------------------


    /**
     * Esegue il filtraggio sulla tabella delle prove.
     */
    filtraProveModelli = () => {
        var value = jQuery('#filtroProveModelli').val().toLowerCase();
        jQuery("#tabProveModelli tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }
    filtraProveArticoli = () => {
        var value = jQuery('#filtroProveArticoli').val().toLowerCase();
        jQuery("#tabProveArticoli tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }
    filtraProveImputazioni = () => {
        var value = jQuery('#filtroProveImputazioni').val().toLowerCase();
        jQuery("#tabProveImputazioni tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }


    getGruppo(val) {
        this.gruppo = val;
    }
    getClasse(val) {
        this.classe = val;
    }
    getGm(val) {
        this.gm = val;
    }
    getSocieta(val) {
        this.societa = val;
        this.attivaBtnCerca = true;
    }

    ricercaTemplateModelli = () => {

        if (jQuery('#tab1 .valido').length >= this.listaTemplate[0]["campi"].length) {
            this.arrayModelli = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            this.arrayModelli = []
        }
    }
    ricercaTemplateArticoli = () => {


        if (jQuery('#tab2 .valido').length >= this.listaTemplate[1]["campi"].length) {

            this.arrayArticoli = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            console.log("NON ci siamo");
            this.arrayArticoli = []
        }
    }
    ricercaTemplateImputazioni = () => {

        if (jQuery('#tab3 .valido').length >= this.listaTemplate[2]["campi"].length) {
            console.log("VALIDO");
            this.arrayImputazioni = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            console.log("NON ci siamo");
            this.arrayImputazioni = [];
        }
    }

    /**
     * Funzione per visualizzare l'alert di conferma del salvataggio.     
     */
    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
    }

    confermaSalvataggio(): void {
        this.message = 'Confirmed!';
        this.modalRef.hide();
    }
    annullaSalvataggio(): void {
        this.message = 'Declined!';
        this.modalRef.hide();
    }

}
