import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';


/*
* Interfaccia che deve rispettare la variabile
* di configurazione da passare all'oggetto.
*/
interface IAutocompleteConfig {
  nome: string;
  values: any[];
}

@Component({
  selector: 'autocompleta',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['../shared.component.scss'],
})

export class AutocompleteComponent implements OnInit {
  constructor() { }
  ngOnInit() {
    //console.log("config: " + JSON.stringify(this.config));
  }

  selected;
  selectedOption: any;
  isSelezione: boolean = false;

  @Input() valore: string;
  @Input() required: boolean;
  @Input() isFiltroTemplate: boolean = false;

  @Input() config?: IAutocompleteConfig = {
    'nome': "Init",
    'values': [
      "Esempio 0",
      "Esempio 1",
      "Esempio 2"
    ],
  };


  @Output() notifyChange: EventEmitter<string> = new EventEmitter<string>();

  @Output() aggiornaValore: EventEmitter<string> = new EventEmitter<string>();

  aggiorna(val) {
    this.aggiornaValore.emit(val);
    this.isSelezione = false;
  }
  selezione(val) {
    console.log("Selezione: " + String(parseInt(val)));
    this.isSelezione = true;
  }

}
