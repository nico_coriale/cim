import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticoliComponent } from './articoli/articoli.component';
import { FormsModule } from '@angular/forms';


// Moduli per il Drag&Drop
//import { DndModule } from 'ngx-drag-drop';
//import { NgxDnDModule } from '@swimlane/ngx-dnd';

import { TabsModule, AccordionModule, AlertComponent } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';
import { SortableModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';


import { RouterModule } from '@angular/router';

// Componenti comuni
import { SharedModule } from '../shared/shared.module';



// SERVIZI
import { CimApiService } from '../services/cim.service';
import { GestioneTemplateComponent } from './gestione-template.component';
import { ModelliComponent } from './modelli/modelli.component';
import { ImputazioniComponent } from './imputazioni/imputazioni.component';



@NgModule({
  declarations: [
    GestioneTemplateComponent,
    ArticoliComponent,
    ModelliComponent,
    ImputazioniComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    
    
    FormsModule,
    RouterModule,
    BsDropdownModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),    
    SortableModule.forRoot(),
    ModalModule.forRoot(),
    
    //DndModule,
    //NgxDnDModule,

    //GestioneTemplateModule,
  ]
})
export class GestioneTemplateModule { }
