import { Component, OnInit } from '@angular/core';
import { DndDropEvent } from 'ngx-drag-drop';


declare let jQuery: any;


@Component({
    selector: 'template-modelli',
    templateUrl: './modelli.component.html',
    styleUrls: ['./modelli.component.scss']
})
export class ModelliComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

    // Dati per popolare i filtri di ricerca
    private anniStagione = [{
        "tipo": "anno_stagione_tagli",
        "chiave": "2011 A/I",
        "valore": "2011 A/I",
        "ordinamento": 20112,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2011 P/E",
        "valore": "2011 P/E",
        "ordinamento": 20111,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2012 A/I",
        "valore": "2012 A/I",
        "ordinamento": 20122,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2012 P/E",
        "valore": "2012 P/E",
        "ordinamento": 20121,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2013 A/I",
        "valore": "2013 A/I",
        "ordinamento": 20132,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2013 P/E",
        "valore": "2013 P/E",
        "ordinamento": 20131,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2014 A/I",
        "valore": "2014 A/I",
        "ordinamento": 20142,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2014 P/E",
        "valore": "2014 P/E",
        "ordinamento": 20141,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2015 A/I",
        "valore": "2015 A/I",
        "ordinamento": 20152,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2015 P/E",
        "valore": "2015 P/E",
        "ordinamento": 20151,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2016 A/I",
        "valore": "2016 A/I",
        "ordinamento": 20162,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2016 P/E",
        "valore": "2016 P/E",
        "ordinamento": 20161,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2017 A/I",
        "valore": "2017 A/I",
        "ordinamento": 20172,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2017 P/E",
        "valore": "2017 P/E",
        "ordinamento": 20171,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2018 A/I",
        "valore": "2018 A/I",
        "ordinamento": 20182,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2018 P/E",
        "valore": "2018 P/E",
        "ordinamento": 20181,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2019 A/I",
        "valore": "2019 A/I",
        "ordinamento": 20192,
        "upd_ts": "2018-08-01 17:36:48",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2019 P/E",
        "valore": "2019 P/E",
        "ordinamento": 20191,
        "upd_ts": "2017-12-12 09:59:25",
        "upd_user": ""
    }
    ];
    private listaSegmentiModelli = [
        {
            "tipo": "segmento_codifiche",
            "chiave": "campionario",
            "valore": "campionario",
            "ordinamento": 0,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "segmento_codifiche",
            "chiave": "produzione",
            "valore": "produzione",
            "ordinamento": 0,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        }
    ];
    private listaClassi = [
        "01 - Cappotto",
        "04 - Giacca",
        "10 - Gonna",
        "22 - Abito",
        "46 - Pelliccia",
    ];
    private listaProve = [
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "Certificazione",
            "valore": "Certificazione/Composizione",
            "ordinamento": 0,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "EcoTox",
            "valore": "EcoTossicologia",
            "ordinamento": 1,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "Fisica",
            "valore": "Fisico-Tecnica",
            "ordinamento": 2,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "Lavaggio",
            "valore": "Lavaggio ",
            "ordinamento": 3,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim_old",
            "chiave": "Infiammabilita",
            "valore": "Infiammabilita'",
            "ordinamento": 4,
            "upd_ts": "2018-08-20 17:23:40",
            "upd_user": "cim3.0"
        }
    ];

    // Variabili di appoggio per le chiamate di richiesta e le risposte 
    private idPreso = '';

    public gruppo?: string;
    public classe?: string;

    public listaTemplate;
    public arrayModelli = [];




    /**
     * CONFIGURAZIONE FILTRI DI RICERCA
     */
    public confAnnoStagione = {
        defaultValue: "Anno Stagione",
        values: this.anniStagione,
        includeAllItem: false,
        required: true,
        campo: "valore",
    };

    public confSegmentiModelli = {
        defaultValue: "Segmento",
        values: this.listaSegmentiModelli,
        includeAllItem: true,
        required: true,
        nomeValoreTutti: "Qualunque"
    };

    public confClasse = {
        nome: "classe",
        placeholder: 'Classe',
        values: this.listaClassi,
        required: true,
        template: "templateClasse",
    };

    public confTipoProva = {
        defaultValue: "Tipo Prova",
        values: this.listaProve,
        includeAllItem: true,
        required: false,
        nomeValoreTutti: "Tutte"
    };



    // METODI

    getClasse(val) {
        this.classe = val;
    }

    ricercaTemplateModelli = () => {

        if (jQuery('#tab1 .valido').length >= this.listaTemplate[0]["campi"].length) {
            this.arrayModelli = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]
        }
        else {
            this.arrayModelli = []
        }
    }

    filtraProveModelli = () => {
        var value = jQuery('#filtroProveModelli').val().toLowerCase();
        jQuery("#tabProveModelli tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }

    svuotaTemplateModelli = () => {
        jQuery('#tabTemplateModelli tbody tr').each(function () {
            jQuery('#tabProveModelli tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateModelli').addClass('is-hidden');
    }




    //  ------------------ DRAG AND DROP 

    draggable = {
        data: "myDragData",
        effectAllowed: "move",
        disable: false,
        handle: false,
    };

    drag = (ev) => {
        //this.idPreso = ev.dataTransfer.setData("text", ev.target.id);
    }
    /**
     * Sposta la riga draggata nel tbody della tabella sulla quale è rilasciata.
     * 
     * N.B. Le righe delle tabelle DEVONO avere un ID, altrimenti non funziona.
     */
    onDrop = (ev) => {
        jQuery("#" + this.idPreso).toggleClass('modifica');
        jQuery(ev.event.target).closest('table').find('tbody').append(
            jQuery("#" + this.idPreso)
        );
    }
    onDragStart = (event) => {
        console.log("drag started", JSON.stringify(event, null, 2));
        this.idPreso = event.srcElement.getAttribute("id");

    }
    onDragEnd = (event: DragEvent) => {
        console.log("drag ended", JSON.stringify(event, null, 2));
    }
    onDragover = (event: DragEvent) => {
    }
}
