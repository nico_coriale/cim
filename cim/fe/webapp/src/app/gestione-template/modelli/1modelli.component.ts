import { Component, OnInit } from '@angular/core';
import { DndDropEvent } from 'ngx-drag-drop';


declare let jQuery: any;


@Component({
  selector: 'template-modelli',
  templateUrl: './modelli.component.html',
  styleUrls: ['./modelli.component.scss']
})
export class ModelliComponent implements OnInit {
  private idPreso = '';
  private arrayArticoli = [];
  private confTipoProva = {};
  private confSegmentiArticoli= {};
  
  constructor() { }

  ngOnInit() {
  }
  
  
  filtraProveArticoli = () => {
    var value = jQuery('#filtroProveArticoli').val().toLowerCase();
    jQuery("#tabProveArticoli tbody tr").filter(function () {
        jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  svuotaTemplateArticoli = () => {
    jQuery('#tabTemplateArticoli tbody tr').each(function () {
        jQuery('#tabProveArticoli tbody').append(jQuery(this));
    });
    jQuery('#btnSalvaTemplateArticoli').addClass('is-hidden');
}





    //  ------------------ DRAG AND DROP 

    draggable = {
      data: "myDragData",
      effectAllowed: "move",
      disable: false,
      handle: false,
  };

  drag = (ev) => {
      //this.idPreso = ev.dataTransfer.setData("text", ev.target.id);
  }
  /**
   * Sposta la riga draggata nel tbody della tabella sulla quale è rilasciata.
   * 
   * N.B. Le righe delle tabelle DEVONO avere un ID, altrimenti non funziona.
   */
  onDrop = (ev) => {
      jQuery("#" + this.idPreso).toggleClass('modifica');
      jQuery(ev.event.target).closest('table').find('tbody').append(
          jQuery("#" + this.idPreso)
      );
  }
  onDragStart = (event) => {
      console.log("drag started", JSON.stringify(event, null, 2));
      this.idPreso = event.srcElement.getAttribute("id");

  }
  onDragEnd = (event: DragEvent) => {
      console.log("drag ended", JSON.stringify(event, null, 2));
  }
  onDragover = (event: DragEvent) => {
  }
}
