import { Component, OnInit } from '@angular/core';
import { DndDropEvent } from 'ngx-drag-drop';


declare let jQuery: any;


@Component({
    selector: 'template-articoli',
    templateUrl: './articoli.component.html',
    styleUrls: ['./articoli.component.scss']
})
export class ArticoliComponent implements OnInit {

    // Dati per popolare i filtri di ricerca
    private anniStagione = [{
        "tipo": "anno_stagione_tagli",
        "chiave": "2011 A/I",
        "valore": "2011 A/I",
        "ordinamento": 20112,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2011 P/E",
        "valore": "2011 P/E",
        "ordinamento": 20111,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2012 A/I",
        "valore": "2012 A/I",
        "ordinamento": 20122,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2012 P/E",
        "valore": "2012 P/E",
        "ordinamento": 20121,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2013 A/I",
        "valore": "2013 A/I",
        "ordinamento": 20132,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2013 P/E",
        "valore": "2013 P/E",
        "ordinamento": 20131,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2014 A/I",
        "valore": "2014 A/I",
        "ordinamento": 20142,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2014 P/E",
        "valore": "2014 P/E",
        "ordinamento": 20141,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2015 A/I",
        "valore": "2015 A/I",
        "ordinamento": 20152,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2015 P/E",
        "valore": "2015 P/E",
        "ordinamento": 20151,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2016 A/I",
        "valore": "2016 A/I",
        "ordinamento": 20162,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2016 P/E",
        "valore": "2016 P/E",
        "ordinamento": 20161,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2017 A/I",
        "valore": "2017 A/I",
        "ordinamento": 20172,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2017 P/E",
        "valore": "2017 P/E",
        "ordinamento": 20171,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2018 A/I",
        "valore": "2018 A/I",
        "ordinamento": 20182,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2018 P/E",
        "valore": "2018 P/E",
        "ordinamento": 20181,
        "upd_ts": "2017-07-03 13:56:07",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2019 A/I",
        "valore": "2019 A/I",
        "ordinamento": 20192,
        "upd_ts": "2018-08-01 17:36:48",
        "upd_user": ""
    },
    {
        "tipo": "anno_stagione_tagli",
        "chiave": "2019 P/E",
        "valore": "2019 P/E",
        "ordinamento": 20191,
        "upd_ts": "2017-12-12 09:59:25",
        "upd_user": ""
    }
    ];
    private segmentiArticoli = [
        {
            "tipo": "segmento_mp",
            "chiave": "campionario",
            "valore": "Campionario",
            "ordinamento": 20,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "segmento_mp",
            "chiave": "capi_prova",
            "valore": "Capi prova",
            "ordinamento": 40,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "segmento_mp",
            "chiave": "inserimento",
            "valore": "Inserimento",
            "ordinamento": 50,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "segmento_mp",
            "chiave": "produzione",
            "valore": "Produzione",
            "ordinamento": 10,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "segmento_mp",
            "chiave": "sfilata",
            "valore": "Sfilata",
            "ordinamento": 30,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        }
    ];
    private listaGM = [
        "10 - Tessuto",
        "13 - Jersey",
        "29 - Lampo",
        "56 - Piumino",
        "84 - Tessuto per accessori",
        "81 - Pelle scarpe e borse"
    ];
    private listaProve = [
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "Certificazione",
            "valore": "Certificazione/Composizione",
            "ordinamento": 0,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "EcoTox",
            "valore": "EcoTossicologia",
            "ordinamento": 1,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "Fisica",
            "valore": "Fisico-Tecnica",
            "ordinamento": 2,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim",
            "chiave": "Lavaggio",
            "valore": "Lavaggio ",
            "ordinamento": 3,
            "upd_ts": "2017-07-03 13:56:07",
            "upd_user": ""
        },
        {
            "tipo": "tipologia_prove_cim_old",
            "chiave": "Infiammabilita",
            "valore": "Infiammabilita'",
            "ordinamento": 4,
            "upd_ts": "2018-08-20 17:23:40",
            "upd_user": "cim3.0"
        }
    ];

    // Variabili di appoggio per le chiamate di richiesta e le risposte 
    public idTemplate = '';
    public templateArticoli = [];

    public gruppo?: string;
    public classe?: string;
    public gm?: string;

    public listaTemplate = [];
    public arrayArticoli = [];


    constructor() { }

    ngOnInit() {
    }


    /**
     *       
     * Search fields - SETUP
     * 
     */
    public confAnnoStagione = {
        defaultValue: "Anno Stagione",
        values: this.anniStagione,
        includeAllItem: false,
        required: true,
        campo: "valore",
    };

    public confSegmentiArticoli = {
        defaultValue: "Segmento",
        values: this.segmentiArticoli,
        includeAllItem: true,
        nomeValoreTutti: "Qualunque",
        required: true,
    };

    public confGM = {
        nome: "gm",
        placeholder: "GM",
        values: this.listaGM,
        required: true,
        template: "templateGM",
    };

    public confTipoProva = {
        defaultValue: "Tipo Prova",
        values: this.listaProve,
        includeAllItem: true,
        required: false,
        nomeValoreTutti: "Tutte"
    };



    // METODI

    getGm(val) {
        this.gm = val;
    }

    ricercaTemplateArticoli = () => {
        if (jQuery('#tab2 .valido').length >= this.listaTemplate.length) {
            this.arrayArticoli = [
                {
                    "id": "44",
                    "nome_prova": "Test Infiammabilità",
                    "descrizione": "Questa è una descrizione di prova"
                },
                {
                    "id": "67",
                    "nome_prova": "Test Veleno",
                    "descrizione": "Questa è sparta"
                }
            ]

            this.idTemplate = '666';
        }
        else {
            console.log("NON ci siamo");
            this.arrayArticoli = []
        }
    }

    filtraProveArticoli = () => {
        var value = jQuery('#filtroProveArticoli').val().toLowerCase();
        jQuery("#tabProveArticoli tbody tr").filter(function () {
            jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
        });
    }

    svuotaTemplateArticoli = () => {
        jQuery('#tabTemplateArticoli tbody tr').each(function () {
            jQuery('#tabProveArticoli tbody').append(jQuery(this));
        });
        jQuery('#btnSalvaTemplateArticoli').addClass('is-hidden');
    }





    //  ------------------ DRAG AND DROP 

    draggable = {
        data: "myDragData",
        effectAllowed: "move",
        disable: false,
        handle: false,
    };

    drag = (ev) => {
        //this.idPreso = ev.dataTransfer.setData("text", ev.target.id);
    }
    /**
     * Sposta la riga draggata nel tbody della tabella sulla quale è rilasciata.
     * 
     * N.B. Le righe delle tabelle DEVONO avere un ID, altrimenti non funziona.
     */
    onDrop = (ev) => {
        jQuery("#" + this.idPreso).toggleClass('modifica');
        jQuery(ev.event.target).closest('table').find('tbody').append(
            jQuery("#" + this.idPreso)
        );
    }
    onDragStart = (event) => {
        console.log("drag started", JSON.stringify(event, null, 2));
        this.idPreso = event.srcElement.getAttribute("id");

    }
    onDragEnd = (event: DragEvent) => {
        console.log("drag ended", JSON.stringify(event, null, 2));
    }
    onDragover = (event: DragEvent) => {
    }
}
