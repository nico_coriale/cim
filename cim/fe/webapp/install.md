

# Angular 5: Quick start

    Run `npm install` to install node dependencies defined in package.json

    Run `npm start` to run the project

# There are also other npm tasks:

    `npm run build:prod` - builds a production version of the application. All html/css/js code is minified and put to dist folder. The contents of this folder you will want to put to your production server when publising the application;

    `npm run lint` - checks your code in .ts files for typescript errors

For more instruction please refer to Angular 5.0 Final seed readme.md.
