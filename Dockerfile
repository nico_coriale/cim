FROM ubuntu:16.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y ubuntu-server


RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:jonathonf/python-3.6
RUN apt-get update
RUN apt-get install -y python3.6 libpython3.6
RUN apt-get install -y python3-pip

RUN apt-get install -y telnet
RUN apt-get install -y wget
RUN apt-get install -y nginx
#RUN apt-get install -y python-pip
RUN apt-get install -y python-mysqldb
RUN apt-get install -y libmysqlclient-dev
RUN pip3 install mysqlclient

COPY default.conf /etc/nginx/conf.d/
RUN rm -f /etc/nginx/sites-enabled/default
RUN systemctl enable nginx

RUN mkdir /usr/src/app/
COPY requirements.txt /usr/src/app/

RUN pip3 install --no-cache-dir -r /usr/src/app/requirements.txt

COPY cim /usr/src/app/

# COPY startup script into known file location in container
COPY start.sh /usr/src/app/

EXPOSE 8000
EXPOSE 80

RUN chmod +x /usr/src/app/start.sh


# CMD specifcies the command to execute to start the server running.
#CMD ["/usr/src/app/start.sh"]

ENTRYPOINT service nginx start && /usr/src/app/start.sh #&& /bin/bash
# done!